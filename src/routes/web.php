<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Default Route 
/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

// Dafault Application Route
Route::get('/', 'HomeController@index')->name('home');
