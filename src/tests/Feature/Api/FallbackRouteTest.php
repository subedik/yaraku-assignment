<?php

namespace Tests\Feature\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FallbackRouteTest extends TestCase
{
    /**
     * A basic API fallback route test.
     *
     * @return void
     */
    public function testMissingAPIRoutesReturnJson404()
    {
        $this->withoutExceptionHandling();
        $response = $this->get('/api/missing/route');
        $response->assertStatus(404);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJson([
            'message' => 'Not Found.'
        ]);
    }
}
