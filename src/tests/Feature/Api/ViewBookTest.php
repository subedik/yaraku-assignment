<?php

namespace Tests\Feature\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Book;

class ViewBookTest extends TestCase
{
    /**
     * Test requesting invalid book triggers module not found exception.
     *
     * @return void
     */
    public function testRequestingInvalidBookTriggersModelNotFoundException()
    {
        $this->json('GET', '/api/books/123')
            ->assertStatus(404)
            ->assertHeader('Content-Type', 'application/json')
            ->assertJson([
                'message' => 'Not Found.'
            ]);
    }

    /**
     * Test requesting invalid book returns no query reslts error.
     *
     * @return void
     */
    public function testRequestingInvalidBookReturnsNoQueryResultsError()
    {
        $this->json('GET', '/api/books/123')
            ->assertStatus(404)
            ->assertHeader('Content-Type', 'application/json')
            ->assertJson([
                'message' => 'Not Found.'
            ]);
    }

    /**
     * Test requesting invalid book url triggers API fallback route.
     *
     * @return void
     */
    public function testInvalidBookUriTriggersFallbackRoute()
    {
        $this->json('GET', '/api/users/invalid-user-id')
            ->assertStatus(404)
            ->assertHeader('Content-Type', 'application/json')
            ->assertJson([
                'message' => 'Not Found.'
            ]);
    }

    /**
     * Test guest can view a valid book.
     *
     * @return void
     */
    public function testGuestsCanViewAValidBook()
    {
        $book = factory(Book::class)->create([
            'title' => 'First Book',
            'author' => 'First Author',

        ]);

        $this->json('GET', "/api/books/" . $book->id)
            ->assertOk()
            ->assertJson([
                'data' => [
                    'id' => 26,
                    'title' => 'First Book',
                    'author' => 'First Author',
                ]
            ]);
    }
}
