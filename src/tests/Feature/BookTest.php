<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Book;

class BookTest extends TestCase
{
    /**
     * Test if the books are created correctly.
     *
     * @return void
     */
    public function testBooksAreCreatedCorrectly()
    {
        $payload = [
            'title' => 'Lorem',
            'author' => 'Ipsum',
        ];

        $this->json('POST', '/api/books', $payload)
            ->assertStatus(201)
            ->assertHeader('Content-Type', 'application/json')
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'title',
                    'author',
                ]
            ])
            ->assertJson([
                'data' =>[
                    'id' => 26,
                    'title' => 'Lorem',
                    'author' => 'Ipsum',
                ]
            ]);
    }

    /**
     * Test if the books are updated correctly.
     *
     * @return void
     */
    public function testBooksAreUpdatedCorrectly()
    {
        $book = factory(Book::class)->create([
            'title' => 'First Book',
            'author' => 'First Author',
        ]);

        $payload = [
            'title' => 'Lorem',
            'author' => 'Ipsum',
        ];

        $this->json('PUT', '/api/books/' . $book->id, $payload)
            ->assertStatus(200)
            ->assertJson([ 
                'data'=> [
                    'id' => 26, 
                    'title' => 'Lorem', 
                    'author' => 'Ipsum' 
                ]
            ]);
    }

    /**
     * Test if the books are deleted correctly.
     *
     * @return void
     */
    public function testBooksAreDeletedCorrectly()
    {
        $book = factory(Book::class)->create([
            'title' => 'First Book',
            'author' => 'First Author',
        ]);

        $this->json('DELETE', '/api/books/' . $book->id, [], [])
            ->assertStatus(200)
            ->assertJson([ 
                'data'=> [
                    'id' => 26, 
                    'title' => 'First Book', 
                    'author' => 'First Author' 
                ]
            ]);
    }

    /**
     * Test if the books are deleted correctly.
     *
     * @return void
     */
    public function testBooksAreListedCorrectly()
    {
        factory(Book::class)->create([
            'title' => 'First Book',
            'author' => 'First Author'
        ]);

        factory(Book::class)->create([
            'title' => 'Second Book',
            'author' => 'Second Author'
        ]);

        $this->json('GET', '/api/books', [], [])
            ->assertStatus(200)
            ->assertJsonFragment(
                [ 
                    'title' => 'First Book', 
                    'author' => 'First Author' 
                ],
                [ 
                    'title' => 'Second Book', 
                    'author' => 'Second Author' 
                ]
            )
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id', 
                        'title', 
                        'author',
                    ]
                ],
            ]);
    }
}
