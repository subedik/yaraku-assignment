@extends('layouts.app')

@section('content')
<v-container fluid>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Books</div>

                <div class="card-body">
                    <book-list></book-list>
                </div>
            </div>
        </div>
    </div>
</v-container>
@endsection
